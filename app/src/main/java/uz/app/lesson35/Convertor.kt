package uz.app.lesson35

import android.graphics.Bitmap
import java.io.ByteArrayOutputStream

class Convertor {
    companion object{
        fun bitmapByteArray(bitmap: Bitmap):ByteArray{
            /**
             * bunda  bitmapdan byteArrayga ogiryapmiz
             * **/
            val stream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG,100,stream)
            val byteArray = stream.toByteArray()
            return byteArray

        }
    }
}