
package uz.app.lesson35

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.app.lesson35.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val manager = supportFragmentManager
        manager.beginTransaction().replace(binding.fragment.id,IntroFragment()).commit()
    }
}