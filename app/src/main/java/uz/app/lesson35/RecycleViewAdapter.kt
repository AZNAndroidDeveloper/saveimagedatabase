package uz.app.lesson35

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecycleViewAdapter( var list: MutableList<User>) :
    RecyclerView.Adapter<RecycleViewAdapter.MyViewHolder>(){
inner  class  MyViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
    val firstName = itemView.findViewById<TextView>(R.id.firstname_tv)
    val lastName = itemView.findViewById<TextView>(R.id.lastname_tv)
    val image = itemView.findViewById<ImageView>(R.id.image)
    fun bind(user: User) {
        /**
         * bu byte arraydan bitmapga otikizish
         * **/
        val bitmap = BitmapFactory.decodeByteArray(user.picture,0,user.picture!!.size)
        firstName.text = user.fName
        lastName.text = user.lName
        image.setImageBitmap(bitmap)


    }
}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(list[position])
    }
}