package uz.app.lesson35

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import uz.app.lesson35.databinding.FragmentIntroBinding
import uz.app.lesson35.db.DatabaseHelper
import java.io.FileNotFoundException


class IntroFragment : Fragment(R.layout.fragment_intro) {
    private lateinit var binding: FragmentIntroBinding

    private lateinit var bitmap:Bitmap
    private lateinit var databaseHelper: DatabaseHelper
    private lateinit var rvAdapter:RecycleViewAdapter
    private var list :MutableList<User> = arrayListOf()
    private lateinit var imageView: ImageView
    private lateinit var dialog: AlertDialog
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        databaseHelper = DatabaseHelper(requireContext())
        list = databaseHelper.readData()
        rvAdapter = RecycleViewAdapter(list)
        val layoutManager = LinearLayoutManager(requireContext(),LinearLayoutManager.VERTICAL,true)
        layoutManager.stackFromEnd = true
        binding.recycleView.layoutManager = layoutManager
        binding.recycleView.adapter = rvAdapter

        binding.btnAdd.setOnClickListener {
            val view = layoutInflater.inflate(R.layout.alert_dialog_item,null,false)
            val et_fName = view.findViewById<EditText>(R.id.firstName_et)
            val et_lName = view.findViewById<EditText>(R.id.lastName_et)
            val btn_save =view.findViewById<Button>(R.id.save_btn)
        imageView = view.findViewById<ImageView>(R.id.image_view)
            val builder = AlertDialog.Builder(requireContext())
            builder.setView(view)
            val dialog = builder.create()
            dialog.show()
            this.dialog = dialog

            imageView.setOnClickListener {
                val intent =Intent(Intent.ACTION_GET_CONTENT)
                intent.type = "image/*"
                startActivityForResult(Intent.createChooser(intent,"Select a picture"),123)
            }
            btn_save.setOnClickListener{
                if(et_fName.text.isNotEmpty() && et_lName.text.isNotEmpty()){
                    val fName = et_fName.text.toString()
                    val lName = et_lName.text.toString()
                    saveUser(fName,lName,bitmap)
                    dialog.dismiss()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==123 && resultCode== Activity.RESULT_OK){
            try {
                /**
                 * input stream korinda bitamap korinishiga olib otyapmiz
                 *
                 * **/
                val inputStream = requireContext().contentResolver.openInputStream(data?.data!!)
                val bitmap = BitmapFactory.decodeStream(inputStream)
                imageView.setImageBitmap(bitmap)
                this.bitmap = bitmap
            }catch (f:FileNotFoundException){
                f.printStackTrace()
            }
        }
    }
    fun saveUser(fName:String,lName:String,bitmap: Bitmap){
        val user = User(0,fName,lName,Convertor.bitmapByteArray(bitmap))
        list.add(user)
        rvAdapter.notifyDataSetChanged()
        databaseHelper.insertData(fName,lName,Convertor.bitmapByteArray(bitmap))

    }
}