package uz.azn.workedwithdatabase.db

class DatabaseContainer {

    companion object{
        val TABLE_NAME ="user_table"
        val COL_ID = "id"
        val FIRST_NAME_COL = "first_name"
        val LAST_NAME_COL = "last_name"
        val IMAGE= "image"
    }
}