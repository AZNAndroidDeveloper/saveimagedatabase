package uz.app.lesson35.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import android.provider.BaseColumns._ID
import uz.app.lesson35.User
import uz.azn.workedwithdatabase.db.DatabaseContainer

val DATABASE_NAME = "user_database"
val DATABE_VERSION = 1
@Suppress("EqualsBetweenInconvertibleTypes")
class DatabaseHelper(context: Context):SQLiteOpenHelper(context, DATABASE_NAME,null, DATABE_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createTable = "CREATE TABLE ${DatabaseContainer.TABLE_NAME}(${DatabaseContainer.COL_ID} INTEGER PRIMARY KEY AUTOINCREMENT," +
                "${DatabaseContainer.FIRST_NAME_COL}  TEXT, ${DatabaseContainer.LAST_NAME_COL} TEXT," +
                "${DatabaseContainer.IMAGE} VAR(256))"
   db!!.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS ${DatabaseContainer.TABLE_NAME}")
    }

    fun insertData(fName:String,lName:String, image :ByteArray){
        val db:SQLiteDatabase = this.writableDatabase
        val sql = "INSERT INTO ${DatabaseContainer.TABLE_NAME} VALUES(NULL, ?, ?, ?)"
        val statement = db.compileStatement(sql)
        statement.clearBindings()
        statement.bindString(1,fName)
        statement.bindString(2,lName)
        statement.bindBlob(3,image)
        statement.executeInsert()
    }

    fun readData():MutableList<User>{
        val list :MutableList<User> = arrayListOf()
        val db = this.writableDatabase
        val query = "SELECT * FROM ${DatabaseContainer.TABLE_NAME}"
        val cursor = db.rawQuery(query,null)
        if (cursor.moveToFirst()){
            do {
                val id = cursor.getInt(0)
                val fName =cursor.getString(1)
                val lName = cursor.getString(2)
                val byteArray = cursor.getBlob(3)
                val user =User(id,fName,lName,byteArray)
                list.add(user)


            }while (cursor.moveToNext())
            cursor.close()

        }
        return list
    }
    }

